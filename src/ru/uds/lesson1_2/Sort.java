package ru.uds.lesson1_2;

import java.util.*;

/**
 * Сортирует заданный метод по указаному формату
 *
 * @author D. Utin 17IT18
 */
public class Sort {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(sort(new int[]{0,10,0,3,3,3,3,3,3,3})));
    }

    /**
     * Сортирует массив по частоте
     *
     * @param array заданный массив
     * @return отсортированный массив array
     */
    static int[] sort(int[] array) {
        Map<Integer, Integer> dictMaximumEntry = new LinkedHashMap<>();
        createAndFillDictMaximumEntryNumber(array, dictMaximumEntry);
        String stringArray = Arrays.toString(array);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (dictMaximumEntry.get(array[j]) < dictMaximumEntry.get(array[j + 1]) ||
                        dictMaximumEntry.get(array[j]).equals(dictMaximumEntry.get(array[j + 1])) &&
                                stringArray.indexOf(array[j] + "") > stringArray.indexOf(array[j + 1] + "")) {
                    int c = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = c;
                }
            }
        }
        return array;
    }

    /**
     * Создает словарь максимального вхождения чисел
     *
     * @param array заданный массив
     */
    private static void createAndFillDictMaximumEntryNumber(int[] array, Map map) {
        for (int value : array) {
            if (!map.containsKey(value)) {
                int count = 0;
                for (int i : array) {
                    if (value == i) {
                        count++;
                    }
                }
                map.put(value, count);
            }
        }
    }
}