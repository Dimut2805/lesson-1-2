package ru.uds.lesson1_2;

import org.junit.Assert;
import org.junit.Test;

public class SortTest {

    @Test
    public void sort() {
        Assert.assertArrayEquals(Sort.sort(new int[]{2, 5, 2, 8, 5, 6, 8, 8}), new int[]{8, 8, 8, 2, 2, 5, 5, 6});
        Assert.assertArrayEquals(Sort.sort(new int[]{2, 5, 2, 6, -1, 9999999, 5, 8, 8, 8}), new int[]{8, 8, 8, 2, 2, 5, 5, 6, -1, 9999999});

    }
}